import React from 'react';

export const AppBackground = ({track}) => {
  const BasicBackground = (
    <div className="App-background App-background--basic"></div>
  );
  if (track == null) { return BasicBackground; }
  let image = track.album.images.find(img => img.height > 200 && img.height < 400);
  if (image == null) { return BasicBackground; }

  return (
    <img
      alt=""
      className="App-background"
      src={image.url} />
  );
};
